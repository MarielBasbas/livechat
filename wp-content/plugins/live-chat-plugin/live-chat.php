<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<?php 
	global $wpdb;
	$current_url = home_url(add_query_arg(array(),$wp->request));
	$logged_in_users = get_transient('online_status');

	// $colours = array('007AFF','FF7000','FF7000','15E25F','CFC700','CFC700','CF1100','CF00BE','F00');
	$user_colour = '5b5b5b';
	$current_user = wp_get_current_user();
	date_default_timezone_set('Asia/Manila');

	echo '<div class="col-md-9 col-xs-12">';

	//select a student - exclusive to admins only
	if(current_user_can('administrator')) {
		if (!empty($logged_in_users)) {
			echo '<form action="#" method="post" name="student-form" id="student-form">
					<label>Select a student</label>
					<select id="current-online-students" name="current-online-students" onchange="this.form.submit()">
					<option disabled selected value>Select an option</option>';
			foreach ( $logged_in_users as $key => $value) {
				if(user_can($key, 'subscriber')) {
					$user = get_user_by('id', $key);
					echo '<option value="'.$key.'">'.$user->first_name.' '.$user->last_name.'</option>';
				}
			}
			echo '</select>
				  </form>';

			if(!isset($_POST['current-online-students'])) {
		    	$user_info = get_userdata($_POST['user_id']);
		    }
		    else {
		    	$user_info = get_userdata($_POST['current-online-students']);
		    }
		    echo '<div class="lc-student">'.$user->first_name.' '.$user->last_name.'</div>';
		} 
		else {
			echo '<div class="lc-student">No current logged in students!</div>';
		}	
	}
	elseif(current_user_can('subscriber')) {
		if(!empty($logged_in_users)) {
			foreach ( $logged_in_users as $key => $value) {
				if(user_can($key, 'administrator')) {
					$connected_admin = get_user_by('id', $key);
					echo '<div class="lc-student">'.$connected_admin->first_name.' '.$connected_admin->last_name.'</div>';
				}
			}
		}
		else {
			echo '<div class="lc-student">No current logged in students!</div>';
		}
	}

	//if the form notes has been set, insert a row in the database
	if(isset($_POST['notes'])){
		$user_id = $_POST['user_id'];
		$councelor = wp_get_current_user();
		$wpdb->insert('wp_live_chat', 
			array( 
				'user_id' => $user_id, 
				'notes' => $_POST['notes'],
				'date' => date("Y-m-d"),
				'time' => date("H:i:s"),
				'councelor' => $councelor->user_firstname.' '.$councelor->user_lastname
			)
		);
	}
?>

<script language="javascript" type="text/javascript">  
	jQuery(document).ready(function($) {
		//create a new WebSocket object.
		var wsUri = "ws://localhost:9000/demo/server.php"; 	
		websocket = new WebSocket(wsUri); 
		
		websocket.onopen = function(ev) { // connection is open 
			$('#message_box').append("<div class=\"system_msg\">Connected!</div>"); //notify user
		}

		$('#send-btn').click(function() { //use clicks message send button	
			var mymessage = $('#message').val(); //get message text
			var myname = $('#name').val(); //get user name

			if(mymessage == "") { //emtpy message?
				alert("Enter Some message Please!");
				return;
			}
			
			var objDiv = document.getElementById("message_box");
			objDiv.scrollTop = objDiv.scrollHeight;
			//prepare json data
			var msg = {
			message: mymessage,
			name: '<?php echo esc_html( $current_user->user_login ); ?>',
			color : '<?php echo $colours[$user_colour]; ?>'
			};
			//convert and send data to server
			websocket.send(JSON.stringify(msg));
		});

		$('#notes-button').click(function(e) { //use clicks message send button	
			var mymessage = $('#notes').val(); //get notes text

			if(mymessage == "") { //emtpy message?
				e.preventDefault();
				$(this).attr('type', 'button')
				alert("Enter some notes!");
				return false;
			}
			else { 
				$(this).attr('type', 'submit');
			}
		});
		
		//#### Message received from server?
		websocket.onmessage = function(ev) {
			var msg = JSON.parse(ev.data); //PHP sends Json data
			var type = msg.type; //message type
			var umsg = msg.message; //message text
			var uname = msg.name; //user name
			var ucolor = msg.color; //color

			if(type == 'usermsg') 
			{
				$('#message_box').append("<div><span class=\"user_name\" style=\"color:#"+ucolor+"\">"+uname+"</span> : <span class=\"user_message\">"+umsg+"</span></div>");
			}
			if(type == 'system')
			{
				$('#message_box').append("<div class=\"system_msg\">"+umsg+"</div>");
			}

			$('input#message').val('');
		};
		
		websocket.onerror	= function(ev){$('#message_box').append("<div class=\"system_error\">Error Occurred - "+ev.data+"</div>");}; 
		websocket.onclose 	= function(ev){$('#message_box').append("<div class=\"system_msg\">Connection Closed</div>");}; 
	});
</script>

<!-- for live chat -->
<div class="chat_wrapper">
	<div class="message_box" id="message_box"></div>
	<div class="panel">
		<input type="text" name="message" id="message" placeholder="Message" maxlength="80" />
		<button id="send-btn" class="button">Send</button>
	</div>
</div>

<?php
	//form for notes
	if((isset($_POST['notes']) || isset($_POST['current-online-students'])) && current_user_can('administrator')) {
		if (!empty($logged_in_users)) {
			echo '<form action="#" method="post" name="notes-form" id="notes-form">
					  <label class="notes">Notes:</label>
					  <div class="chat_wrapper"><textarea name="notes" id="notes" placeholder="Type in your notes..."></textarea></div>
					  <button type="submit" class="button" name="notes-button" id="notes-button">Save</button>';
		    if(!isset($_POST['current-online-students'])) {
		    	$row = $wpdb->get_results( "SELECT * FROM wp_live_chat WHERE user_id = '$user_id' ORDER BY id DESC");
		  		echo '<input type="hidden" name="user_id" value="'.$_POST['user_id'].'"></form>';
		    }
		    else {
		    	$user_id = $_POST['current-online-students'];
		    	$row = $wpdb->get_results( "SELECT * FROM wp_live_chat WHERE user_id = '$user_id' ORDER BY id DESC");
		  		echo '<input type="hidden" name="user_id" value="'.$user_id.'"></form>';
		    }

		    echo '</div>';

		    //list of student's notes
		    echo '<div class="student-record col-md-3 col-xs-12">';
		    echo '<label class="notes">Student Record:</label><div class="chat_wrapper">';
		    echo '<div class="record-wrapper">';
		    foreach ($row as $row) { 
		    	echo '<div class="record-item">';
		    	echo '<div class="date-time"><p><b>Date:</b> '.$row->date.'</p>';
		    	echo '<p><b>Time:</b> '.date('g:ia' ,strtotime($row->time)).'</p>';
		    	echo '<p><b>Councelor:</b> '.$row->councelor.'</p></div>';
		    	echo '<p>'.$row->notes.'</p></div>';
		    } 
		    echo '</div></div></div>';
		} 
	}

	if(!isset($_POST['current-online-students'])) {
		echo '</div>';
	}

	if(current_user_can('subscriber')) {
		echo '<div class="student-record col-md-3 col-xs-12">';
		echo '<label class="notes">NOTE:</label>';
		echo '<p>All conversations are kept confidential and will be deleted upon termination of your session.</p>';
		echo '<p>The FEU Tech Online Counseling System will not be held accountable for any damages to trust and relationships between you and your family, friends, or significant others.</p>';
		echo '<p>Please do not share sensitive information with your counselor. These may include but are not limited to social security number, credit card numbers, login information, and the name of your first ex.</p>';
		echo '</div>';
	}
?>