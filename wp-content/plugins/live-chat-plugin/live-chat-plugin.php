<?php
/**
* @package LiveChatPlugin
*/
/*
Plugin Name: Live Chat Plugin
Plugin URI: https://feuguidanceunit.000webhostapp.com/
Description: Live Chat Plugin for FEU Tech Online Counseling
Version: 1.0.0
Author: FEU
Author URI: https://feuguidanceunit.000webhostapp.com/
License: GPLv2 or later
Text Domain: live-chat-plugin-mari
*/

/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//when someone tries to access your files outside of your website
defined('ABSPATH') or die('Do not access my files!');

class LiveChatPlugin {
	function __construct() {	
		add_shortcode('live_chat', array($this, 'live_chat')); //adding shortcode so it can be added to a page. shortcode is hooked to live_chat() function
		add_action( 'wp', array($this, 'user_online_update')); //executing a function
		add_action('clear_auth_cookie', array($this, 'clear_transient_on_logout')); //executing a function
	}

	//when the plugin is activated
	function activate() {
		flush_rewrite_rules(); //reinitialized
	}

	//add js and css
	function register() {
		add_action('wp_enqueue_scripts', array($this, 'enqueue'));
	}

	//when the plugin is deactivated
	function deactivate() {
		flush_rewrite_rules();
	}

	function live_chat() {
		include 'live-chat.php';
	}

	//call js and css
	function enqueue() {
		wp_enqueue_style('frontendstyle', plugins_url('/assets/css/front-end.css', __FILE__));
		wp_enqueue_script('frontendscript', plugins_url('/assets/js/front-end.js', __FILE__));
	}

	//check online users
	function  user_online_update(){
		if (is_user_logged_in()) {
			// get the user activity the list
			$logged_in_users = get_transient('online_status');

			// get current user ID
			$user = wp_get_current_user();

			// check if the current user needs to update his online status;
			// he does if he doesn't exist in the list
			$no_need_to_update = isset($logged_in_users[$user->ID])

			    // and if his "last activity" was less than let's say ...1 minutes ago
			    && $logged_in_users[$user->ID] >  (time() - (1 * 60));

			// update the list if needed
			if(!$no_need_to_update) {
				$logged_in_users[$user->ID] = time();
				set_transient('online_status', $logged_in_users, (2*60)); // 2 mins
			}
		}
	}

	//clear online the online user on the list when one is logged out
	function clear_transient_on_logout() {
		$user_id = get_current_user_id();

		$users_transient_id = get_transient('online_status');

		if(is_array($users_transient_id)){
			foreach($users_transient_id as $id => $value ){
				if ( $id == $user_id ) {
					unset($users_transient_id[$user_id]);
					set_transient('online_status', $users_transient_id, (2*60)); // 2 mins
					break;
				}
			}
		}else{
			delete_transient('online_status');
		}
	}
}

if(class_exists('LiveChatPlugin')) {
	$liveChatPlugin  = new LiveChatPlugin();
	$liveChatPlugin->register();
}

//activation
register_activation_hook(__FILE__, array($liveChatPlugin, 'activate'));

//deactivation
register_deactivation_hook(__FILE__, array($liveChatPlugin, 'deactivate'));