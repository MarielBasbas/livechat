<div class="d4p-group d4p-group-changelog">
    <h3>de_DE (German)</h3>
    <div class="d4p-group-inner">
        <ul>
            <li><?php _e("Version", "gd-bbpress-toolbox"); ?>: <span>2.3</span></li>
            <li><?php _e("Translator", "gd-bbpress-toolbox"); ?>: <span>Martin Pohle</span></li>
            <li><?php _e("URL", "gd-bbpress-toolbox"); ?>: <span><a target="_blank" href="https://www.foto-video-berlin.de/">https://www.foto-video-berlin.de/</a></span></li>
        </ul>
    </div>
</div>
