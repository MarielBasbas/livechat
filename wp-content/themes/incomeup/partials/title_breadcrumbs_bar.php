<?php
	global $ABdev_title_bar_title;

?>

<?php if(!get_theme_mod('hide_title_breadcrumbs_bar', false)): ?>
	<h1><?php echo (!empty($ABdev_title_bar_title)) ? $ABdev_title_bar_title : get_the_title();?></h1>
<?php endif; ?>