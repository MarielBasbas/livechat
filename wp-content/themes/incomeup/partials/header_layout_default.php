<?php
	$class_invert = (get_theme_mod( 'header_style_invert', false)) ? 'dark_menu_style' : '';
	$sticky_header_class = (get_theme_mod( 'header_with_sticky', false)) ? 'sticky_main_header' : '';
	$current_user = wp_get_current_user();
?>

<?php if(!is_front_page()) { ?>
<?php 
	$logged_in_users = get_transient('online_status');

	foreach ( $logged_in_users as $key => $value) {		
		if(user_can($key, 'administrator')) {
			echo '<div class="online-admin">'.count($logged_in_users).' Online Counselor/s</div>';
		}
	}
?>
<header id="ABdev_main_header" class="clearfix default <?php echo esc_attr($class_invert) .' '. esc_attr($sticky_header_class) ?> ">
	<div id="logo_menu_bar">
		<div class="container menu_container">
			<div class="left">
				<div id="logo">
					<a href="<?php echo home_url(); ?>">
						<?php
						$header_logo = get_theme_mod('header_logo', '');
						$header_retina_logo = get_theme_mod('header_retina_logo', '');
						$header_retina_logo_width = get_theme_mod('header_retina_logo_width', '');
						$header_retina_logo_height = get_theme_mod('header_retina_logo_height', '');
						if( $header_logo!='' ):?>
							<img id="main_logo" src="<?php echo $header_logo;?>" alt="<?php bloginfo('name');?>">
						<?php if( $header_retina_logo!='' &&  $header_retina_logo_width!='' && $header_retina_logo_height!='' ):?>
							<?php $pixels ="";
							if(is_numeric($header_retina_logo_width) && is_numeric($header_retina_logo_height)):
							$pixels ="px";
							endif; ?>
							<img id="retina_logo" src="<?php echo $header_retina_logo;?>" alt="<?php bloginfo('name');?>" style="width:<?php echo $header_retina_logo_width.$pixels; ?>;max-height:<?php echo $header_retina_logo_height.$pixels; ?>; height: auto !important;">
						<?php endif; ?>
						<?php else: ?>
							<h1 id="main_logo_textual"><?php bloginfo('name');?></h1>
							<?php $blog_description = get_bloginfo('description');
							if( $blog_description!='' ): ?>
								<h2 id="main_logo_tagline"><?php echo $blog_description;?></h2>
							<?php endif; ?>
						<?php endif; ?>
					</a>
				</div>
				<?php if(!get_theme_mod('hide_search', true)): ?>
					<div class="search-toggle">
						<a class="search-icon"><i class="ci_icon-search"></i></a>
						<div id="search-container" class="search-box-wrapper hide">
	    					<div class="search-box">
	    					    <?php get_template_part('partials/header_search_default');  ?>
	    					</div>
						</div>
					</div>
				<?php endif; ?>
				<nav>
					<?php wp_nav_menu( array( 'theme_location' => 'header-menu','container' => false,'menu_id' => 'main_menu','menu_class' => '','walker'=> new incomeup_walker_nav_menu, 'fallback_cb' => false ) );?>
				</nav>
				<div id="ABdev_menu_toggle"><i class="ci_icon-menu"></i></div>
			</div>

			<div class="user">
				<div class="user-name"><?php echo $current_user->user_firstname.' '.$current_user->user_lastname; ?></div>
				<div class="user-picture"><?php mt_profile_img(0); ?></div>
			</div>
		</div>
	</div>
</header>

<div id="ABdev_header_spacer"></div>

<?php } ?>