<?php
$values = get_post_custom( $post->ID );

get_header();

get_template_part('partials/header_menu');

?>

<?php if(isset($values['ABdev_no_container'][0]) && $values['ABdev_no_container'][0] == 1):?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
		<?php the_content();?>
	<?php endwhile; endif;?>
<?php elseif(is_front_page()): ?>
	<div id="default_page_row" class="container container-front-page">
		<div class="row">
			<div class="login-info">
				<div class="feu-logo"><img src="<?php echo TEMPPATH.'/images/LoginLogo.png'; ?>" class="img-responsive" /></div>
				<div class="title"><img src="<?php echo TEMPPATH.'/images/LoginTitle.png'; ?>" class="img-responsive" /></div>
				<p class="description">FEU-Tech's Online Counseling System is a way for students to get in touch with and seek help from our counselors at the Guidance Office without physical confrontation</p>
			</div>
			<div class="login-form">
				<h1>Log in</h1>
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
					<?php the_content();?>
				<?php endwhile; endif;?>
			</div>
		</div>

	</div>
</section>
<?php else: ?>
		<div id="default_page_row" class="container">
			<!-- <?php if( isset($values['ABdev_hide_breadcrumbs'][0]) && $values['ABdev_hide_breadcrumbs'][0] == 0){
				get_template_part('partials/title_breadcrumbs_bar');
			} ?> -->
			<div class="row">
				
				<!-- <div class="<?php echo (isset($values['ABdev_page_layout'][0])) ? esc_attr($values['ABdev_page_layout'][0]) : '' ;?> <?php echo (isset($values['ABdev_page_layout'][0]) && $values['ABdev_page_layout'][0]=='full_width' )?'span12 content':'span9 content';?> <?php echo (isset($values['ABdev_page_layout'][0]) && $values['ABdev_page_layout'][0]=='left_sidebar')?'content_with_left_sidebar': ( (isset($values['ABdev_page_layout'][0]) && $values['ABdev_page_layout'][0]=='right_sidebar') ?'content_with_right_sidebar' : '');?>"> -->
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post();?>
						<?php the_content();?>
					<?php endwhile; endif;?>
				<!-- </div> -->

				<?php if (isset($values['ABdev_page_layout'][0]) && in_array($values['ABdev_page_layout'][0], array('left_sidebar','right_sidebar'))) : ?>
				<aside class="span3 sidebar <?php echo (isset($values['ABdev_page_layout'][0]) && $values['ABdev_page_layout'][0]=='left_sidebar' )?'sidebar_left':'sidebar_right';?>">
					<?php get_sidebar(); ?>
				</aside>
				<?php endif; ?>
			</div>

		</div>
	</section>
<?php endif; ?>

<?php get_footer();